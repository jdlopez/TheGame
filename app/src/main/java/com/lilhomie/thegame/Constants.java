package com.lilhomie.thegame;

import android.content.Context;

/**
 * Created by jdlopez on 1/15/2018.
 */

public class Constants {
    public static int SCREEN_WIDTH;
    public static int SCREEN_HEIGHT;

    public static Context CURRENT_CONTEXT;

    public static long INIT_TIME;
}
