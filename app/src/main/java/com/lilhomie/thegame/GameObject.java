package com.lilhomie.thegame;

import android.graphics.Canvas;

/**
 * Created by jdlopez on 1/14/2018.
 */

public interface GameObject {
    public void draw(Canvas canvas);

    public void update();
}
